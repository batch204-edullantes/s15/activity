/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

let firstName = 'Jim Christian';
let lastName = 'Edullantes';
let age = 23;
const hobbies = ['Playing Guitar', 'Watching Movies', 'Playing Online Games'];
const workAddress = {
	'houseNumber': '1089' ,
	'street': 'Salawagan',
	'city': 'Quezon',
	'state': 'Bukidnon'
};

console.log('First Name: ' + firstName);
console.log('Last Name: ' + lastName);
console.log('Age: ' + age);
console.log('Hobbies:');
console.log(hobbies);
console.log('Work Address:');
console.log(workAddress);

	let fullName = "Steve Rogers";
	console.log("My full name is" + name);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	const friends = ["Tony","Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	const profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);



